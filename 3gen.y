    %{
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    int yylex();
    void yyerror(const char *);

    static struct {
        int while_loop;
        int cond_if;
        int for_loop;
        int var;
    } counter;

    // %right '='

    #include "y.tab.h"
%}

%union {
    char str[128];
    int count;
}

%expect 34

%token<str> LGL_AND LGL_NOT LGL_OR LGL_XOR  // logical operators
%token<str> BIN_AND BIN_NOT BIN_OR BIN_XOR  // bitwise operators

%token<str> LT LTE GT GTE EQ NEQ            // relation operators

%token<str> INTEGER FLOAT                   // numeric constants

%token<str> TYPE_FLOAT TYPE_INT             // types

%token<str> VARIABLE                        // vars

%token<count> LOOP_FOR LOOP_WHILE                  // loops
%token<count> COND_IF                              // control structures

%type<str> expr term eqexpr optexpr         // non-terminals with synth attrs
%type stmts stmt blocks block               // non-terminals without attrs

%left EQ NEQ
%left LT LTE GT GTE
%left LGL_OR
%left LGL_XOR
%left LGL_AND
%left BIN_OR
%left BIN_XOR
%left BIN_AND
%left '+' '-'
%left '*' '/' '%'

%start program

%%
program:
    blocks           {}
    | %empty         {}
    ;

blocks:
    blocks block     {}
    | block          {}
    ;

block:
    '{'                         {printf("{\n");}
    stmts '}'                   {printf("\n}");}
    | stmt                      {}

stmts:
  stmts stmt                    {}
  |stmt                         {}

stmt:
  optexpr ';'              {puts("");}
  | COND_IF '(' expr ')'   {$1 = counter.cond_if++;
                            printf("if (!(%s)) goto LBL_IF_RST%d;\n", $3, $1);}
    block                  {printf("\nLBL_IF_RST%d:", $1);}
  | LOOP_WHILE '('            {$1 = counter.while_loop++;
                               printf("\nLBL_WHILE_BEG%d: {", $1);}
                   expr ')'   {printf("if (!(%s)) "
                                      "goto LBL_WHILE_RST%d;}\n", $4, $1);}
    block                     {printf("goto LBL_WHILE_BEG%d;\n"
                                      "\nLBL_WHILE_RST%d:", $1, $1);}
  | LOOP_FOR '('     {$1 = counter.for_loop++;
                      printf("{\n");}
        optexpr ';'  {printf("\nLBL_LOOP_BGN%d:{\n", $1);}
        optexpr ';'  {printf("if (!(%s)) goto LBL_LOOP_RST%d;"
                              "\ngoto LBL_LOOP_BODY%d;}\nLBL_LOOP_STMT%d:{",
                              $7, $1, $1, $1);}
        optexpr ')'  {printf("\n}goto LBL_LOOP_BGN%d;\nLBL_LOOP_BODY%d:", $1, $1);}
    block            {printf("\ngoto LBL_LOOP_STMT%d;\n}\nLBL_LOOP_RST%d:", $1, $1);}
  ;


optexpr:
  expr                       {strcpy($$, $1);}
  | %empty                   {strcpy($$, "1");}
  ;

expr:
  eqexpr                     {strcpy($$, $1);}
  | term                     {strcpy($$, $1);}
  ;

eqexpr:
  VARIABLE '=' expr          {strcpy($$, $1); printf("%s = %s;", $1, $3);}
  ;

term:
  '(' expr ')'               {strcpy($$, $2);}

  | term LT term             {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, $2, $3);}
  | term GT term             {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, $2, $3);}
  | term LTE term            {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, $2, $3);}
  | term GTE term            {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, $2, $3);}
  | term EQ term             {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, $2, $3);}
  | term NEQ term            {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, $2, $3);}

  | term LGL_OR term         {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, $2, $3);}
  | term LGL_XOR term        {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, $2, $3);}
  | term LGL_AND term        {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, $2, $3);}

  | term BIN_OR term         {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, $2, $3);}
  | term BIN_XOR term        {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, $2, $3);}
  | term BIN_AND term        {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, $2, $3);}

  | BIN_NOT term             {sprintf($$, "t%d", counter.var++); printf("int %s = %s%s;", $$, $1, $2);}
  | LGL_NOT term             {sprintf($$, "t%d", counter.var++); printf("int %s = %s%s;", $$, $1, $2);}

  | term '*' term            {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, "*", $3);}
  | term '/' term            {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, "/", $3);}
  | term '%' term            {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, "%", $3);}
  | term '+' term            {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, "+", $3);}
  | term '-' term            {sprintf($$, "t%d", counter.var++); printf("int %s = %s %s %s;", $$, $1, "-", $3);}

  | INTEGER                  {strcpy($$, $1);}
  | FLOAT                    {strcpy($$, $1);}
  | VARIABLE                 {strcpy($$, $1);}
  ;
%%

void yyerror(const char *s) {
  fprintf(stderr, "ERROR: %s\n", s);
}

int yywrap() {return 1;}

int main() {
  return yyparse();
}
