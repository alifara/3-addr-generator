all: default

default: 3gen

yacc: 3gen.y Makefile
	yacc  --warnings=all -d 3gen.y

lex: 3gen.lex Makefile
	lex 3gen.lex

3gen: lex yacc y.tab.c y.tab.h lex.yy.c
	gcc -Wall -Wextra -o 3gen.o y.tab.c lex.yy.c -D YYDEBUG=1

run: 3gen
	./3gen.o


clean:
	$(RM) lex.yy.c
	$(RM) 3gen.o
	$(RM) y.tab.c y.tab.h
