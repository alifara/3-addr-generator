# 3Gen

A 3 address code generator written in `flex` and `bison`.

3 address code is a simple subset of C programming language described in the dragon book.


Have a look at the `Makefile` for building and running the compiler.
